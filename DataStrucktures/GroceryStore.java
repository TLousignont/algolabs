import java.util.Iterator;

import edu.neumont.util.Client;
import edu.neumont.util.QueueableService;


public class GroceryStore implements QueueableService {

	public GroceryLine[] lines;
	public GroceryStore() {
		// TODO Auto-generated constructor stub
	}

	public GroceryStore(int numberOfLines) {
		lines = new GroceryLine[numberOfLines];
		for(int i = 0; i < lines.length; i++){
			lines[i] = new GroceryLine();
		}
	}
	
	@Override
	public double getAverageClientWaitTime() {
		double totalTime = 0.0f;
		int numClients = 0;
		for(int i = 0; i < lines.length; i++){
			Iterator<Client> ci = lines[i].line.iterator();
			while(ci.hasNext()){
				Client nex = ci.next();
				totalTime += getClientWaitTime(nex);
			}
			numClients += lines[i].numClients;
		}
		double ave = totalTime/numClients;
		return ave;
	}

	@Override
	public double getClientWaitTime(Client client) {
		double waitTime = 0;
		boolean foundClient = false;
		for(int i = 0; i < lines.length; i++){
			Iterator<Client> ci = lines[i].line.iterator();
			while(ci.hasNext()){
				Client nex = ci.next();
				if(!foundClient){
					if(nex == client){
						foundClient = true;
					}
					else 
					{
						waitTime = waitTime + nex.getExpectedServiceTime();
					}
				}
			}
			if(!foundClient) {
				waitTime = 0;
			}
		}
		return waitTime;
	}

	@Override
	public boolean addClient(Client client) {
		int lineNumber = 0;
		double waitTimeforLine = lines[0].totalWaitTime;
		for(int i = 0; i < lines.length; i++){
			if(lines[i].totalWaitTime <= waitTimeforLine){
				lineNumber = i;
				waitTimeforLine = lines[i].totalWaitTime;
			}
		}
		lines[lineNumber].line.add(client);
		lines[lineNumber].totalWaitTime += client.getExpectedServiceTime();
		lines[lineNumber].numClients++;
		return true;
	}

	@Override
	public void advanceMinute() {
		for(int i = 0; i < lines.length; i++){
			Iterator<Client> ci = lines[i].line.iterator();
			if(lines[i].front == null || lines[i].front.getExpectedServiceTime() <= 0){
				if(ci.hasNext()){
					lines[i].front = ci.next();
				}
			}
			else if(lines[i].front != null) {
				lines[i].front.servedMinute();
			}
			
			Iterator<Client> update = lines[i].line.iterator();
			while(update.hasNext()){
				lines[i].totalWaitTime = lines[i].totalWaitTime + update.next().getExpectedServiceTime();
			}
		}
	}

}
