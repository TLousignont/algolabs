import java.util.Iterator;

import edu.neumont.util.List;


public class ArrayList<T> implements List<T> {

	Object[] array = new Object[10];
	int currentIndex = 0;
	@Override
	public Iterator<T> iterator() {
		Iterator<T> iterate = new Iterator<T>() {
			int currentIndex = -1;
			@Override
			public boolean hasNext() {
				return (array[currentIndex+1] != null);
			}

			@Override
			public T next() {
				T temp = (T)array[currentIndex+1];
				currentIndex++;
				return temp;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
			}
		};
		return iterate;
	}

	@Override
	public boolean add(T t) {
		if(currentIndex >= array.length){
			Object[] tempArray = new Object[array.length*2];
			for(int i = 0; i < array.length;i++){
				tempArray[i] = array[i];
			}
			array = tempArray;
		}
		array[currentIndex] = t;
		currentIndex++;
		return true;
	}

	@Override
	public T get(int index) {
		if(index < 0 || index > array.length){
			throw new IndexOutOfBoundsException("No item at index");
		}
		return (T)array[index];
	}

	@Override
	public boolean remove(T t) {
		Object[] tempArray = new Object[array.length];
		int addIndex = 0;
		boolean foundRemove = false;
		for(int i = 0; i < array.length; i ++){
			if((T)array[i] != t){
				tempArray[addIndex] = array[i];
				addIndex++;
			}
			else {
				foundRemove = true;
			}
		}
		array = tempArray;
		return foundRemove;
	}

	@Override
	public int size() {
		return array.length;
	}
	/**
	 * @param args
	 */

}
