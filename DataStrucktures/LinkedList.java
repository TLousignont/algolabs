import java.util.Iterator;

import edu.neumont.util.Queue;

public class LinkedList<T> implements Queue<T> {

	Node head = null;
	Node tail = null;
	boolean firstItem = true; 
	@Override
	public Iterator<T> iterator() {
		Iterator<T> iterate = new Iterator<T>() {
			Node start = new Node(new Object(), head);
			Node current = start;
			@Override
			public boolean hasNext() {
				return (current.nextNode != null);
			}

			@Override
			public T next() {
				T temp = (T)current.nextNode.nodeObject;
				current = current.nextNode;
				return temp;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
			}
		};
		return iterate;
	}

	@Override
	public T poll() {
		if(head == null) {
			throw new NullPointerException("Link list is empty.");
		}
		T value = (T)head.nodeObject;
		head = head.nextNode;
		return value;
	}

	@Override
	public boolean offer(T t) {
		if(firstItem) {
			head = new Node(t, null);
			tail = head;
			firstItem = false;
		}
		else {
			Node newNode = new Node(t, null);
			tail.nextNode = newNode;
			tail = newNode;
		}
		return false;
	}

	@Override
	public T peek() {
		if(head == null) {
			throw new NullPointerException("Link list is empty.");
		}
		T value = (T)head.nodeObject;
		return value;
	}

}
