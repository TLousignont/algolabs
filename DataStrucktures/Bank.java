import java.util.Iterator;

import edu.neumont.util.Client;
import edu.neumont.util.QueueableService;


public class Bank implements QueueableService{

	public LinkedList<Client> clients = new LinkedList<Client>();
	public Client Tellers[]; 
	
	public Bank() {
		// TODO Auto-generated constructor stub
	}
	public Bank(int numberOfTellers){
		Tellers = new Client[numberOfTellers];
	}
	
	public void advanceMinute(){
		for(int i = 0; i < Tellers.length; i ++) {
			Iterator<Client> ci = clients.iterator();
			if(Tellers[i] != null){
				if(Tellers[i].servedMinute() <= 0){
					if(ci.hasNext()){
						Tellers[i] = clients.poll();
					}
					else {
						Tellers[i] = null;
					}
				}	
			}
			else {
				if(ci.hasNext()){
					Tellers[i] = clients.poll();
				}
			}
		}
	}
	
	public void addCustomer(Client client) {
		clients.offer(client);
	}
	
	public double getClientWaitTime(Client client) {
		Iterator<Client> ci = clients.iterator();
		double[] tellerLinetimes = new double[Tellers.length];
		for(int i = 0; i < Tellers.length; i ++){
			if(Tellers[i] != null){
				tellerLinetimes[i] = Tellers[i].getExpectedServiceTime();
			}
		}
		double waitTime = tellerLinetimes[0];
		boolean found = false;
		while(ci.hasNext() && !found){
			Client clientAt = ci.next();
			if(clientAt == client){
				found = true;
			}
			int targetTeller = 0;
			for(int i = 0; i < Tellers.length; i ++) {
					if(tellerLinetimes[i] <= waitTime){
						waitTime = tellerLinetimes[i];
						targetTeller = i;
				}
			}
			waitTime =tellerLinetimes[targetTeller];
			tellerLinetimes[targetTeller] += clientAt.getExpectedServiceTime();
		}
		return waitTime;
	}
	
	public double getAverageClientWaitTime() {
		int numClients = 0;
		double totalWaitTime = 0;
		
		Iterator<Client> ci = clients.iterator();
		while(ci.hasNext()){
			Client c = ci.next();
			numClients++;
			double waitTime = getClientWaitTime(c);
			totalWaitTime = totalWaitTime + waitTime;
		}
		double averageTime = totalWaitTime/numClients;
		return averageTime;
	}
	
	@Override
	public boolean addClient(Client client) {
		clients.offer(client);
		return true;
	}

}
