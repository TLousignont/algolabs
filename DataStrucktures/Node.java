

public class Node {

	public Node nextNode;
	public Object nodeObject;
	
	public Node() {
		
	}
	
	public Node(Object input, Node connection) {
		nextNode = connection;
		nodeObject = input;
	}
}
