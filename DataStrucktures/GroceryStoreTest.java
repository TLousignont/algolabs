import static org.junit.Assert.*;

import org.junit.Test;

import edu.neumont.util.Client;


public class GroceryStoreTest {

	@Test
	public void constructStore() {
		GroceryStore store = new GroceryStore(2);
		assertEquals("There are 2 lines in the store", 2, store.lines.length);
	}
	
	@Test
	public void addClient() {
		GroceryStore store = new GroceryStore(2);
		Client client = new Client(10);
		store.addClient(client);
		assertEquals("Client added to line 1: ", client, store.lines[1].line.get(0));
	}
	
	@Test
	public void addClient2() {
		GroceryStore store = new GroceryStore(2);
		Client client = new Client(10);
		Client client2 = new Client(10);
		store.addClient(client);
		store.addClient(client2);
		assertEquals("Client added to line 2: ", client2, store.lines[0].line.get(0));
	}
	
	@Test
	public void getWaitTime1() {
		GroceryStore store = new GroceryStore(2);
		Client client = new Client(10);
		store.addClient(client);
		double waitTime = store.getClientWaitTime(client);
		assertEquals("Wait Time of customer 1 is 0: ", 0, waitTime, 0.1);
	}
	
	@Test
	public void getWaitTime2() {
		GroceryStore store = new GroceryStore(2);
		Client client = new Client(10);
		Client client2 = new Client(10);
		Client client3 = new Client(10);
		store.addClient(client);
		store.addClient(client2);
		store.addClient(client3);
		double waitTime = store.getClientWaitTime(client3);
		assertEquals("Wait Time of customer 3 is 10: ", 10, waitTime, 0.1);
	}
	
	@Test
	public void aveTime() {
		GroceryStore store = new GroceryStore(2);
		Client client = new Client(30);
		Client client2 = new Client(30);
		Client client3 = new Client(10);
		store.addClient(client);
		store.addClient(client2);
		store.addClient(client3);
		double aveTime = store.getAverageClientWaitTime();
		assertEquals("avreageWaitTime is 10: ", 10, aveTime, 0.1);
	}
	
	@Test
	public void advanveMinuetTest1() {
		GroceryStore store = new GroceryStore(2);
		Client client = new Client(10);
		store.addClient(client);
		store.advanceMinute();
		assertEquals("client 1 is now in front of line: ", store.lines[1].front, client);
	}
	
	@Test
	public void advanveMinuetTest2() {
		GroceryStore store = new GroceryStore(2);
		Client client = new Client(10);
		Client client2 = new Client(10);
		store.addClient(client);
		store.addClient(client2);
		store.advanceMinute();
		store.advanceMinute();
		assertEquals("Client's time is now 9 ", 9, client.getExpectedServiceTime());
	}
}
