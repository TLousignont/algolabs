import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;


public class ArrayListTest {

	
	@Test
	public void AlAdd() {
		ArrayList<String> AL = new ArrayList<String>();
		AL.add("1");
		assertEquals("Added 1: ","1",AL.get(0)); 
	}
	
	@Test
	public void ALget() {
		ArrayList<String> AL = new ArrayList<String>();
		AL.add("1");
		AL.add("2");
		AL.add("3");
		assertEquals("The second item is 2: ","2",AL.get(1)); 
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void GetOutOfBounds() {
		ArrayList<String> AL = new ArrayList<String>();
		AL.get(11);
	}
	
	@Test
	public void ALremove() {
		ArrayList<String> AL = new ArrayList<String>();
		AL.add("1");
		AL.add("2");
		AL.add("3");
		assertEquals("removed item 2: ",true,AL.remove("2")); 
	}
	
	@Test
	public void ALIhasNext() {
		ArrayList<String> AL = new ArrayList<String>();
		AL.add("1");
		Iterator<String> is = AL.iterator();
		assertEquals("has next is true: ",true,is.hasNext()); 
	}
	
	@Test
	public void ALINext() {
		ArrayList<String> AL = new ArrayList<String>();
		AL.add("1");
		Iterator<String> is = AL.iterator();
		assertEquals("next Item is 1 ","1",is.next()); 
	}
	
	@Test
	public void ALGrowth() {
		ArrayList<String> AL = new ArrayList<String>();
		for(int i = 0; i < 11; i++){
			AL.add(Integer.toString(i));
		}
		assertEquals("The array length is 20 ",20,AL.size()); 
	}
}
