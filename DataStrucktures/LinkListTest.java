import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;


public class LinkListTest {

	

	@Test(expected=NullPointerException.class)
	public void LLNullPeek() {
		LinkedList<String> LL = new LinkedList<String>();
		LL.peek();
	}
	
	@Test(expected=NullPointerException.class)
	public void LLNullPoll() {
		LinkedList<String> LL = new LinkedList<String>();
		LL.poll();
	}
	
	@Test
	public void LLOffer() {
		LinkedList<String> LL = new LinkedList<String>();
		LL.offer("1");
		assertEquals("First string is 1","1", LL.peek());
	}
	
	@Test
	public void LLPoll() {
		LinkedList<String> LL = new LinkedList<String>();
		LL.offer("1");
		assertEquals("Polled item is 1","1", LL.poll());
	}
	
	@Test
	public void LLPollTwice() {
		LinkedList<String> LL = new LinkedList<String>();
		LL.offer("1");
		LL.offer("2");
		LL.poll();
		String secondPoll = LL.poll();
		assertEquals("Second Polled item is 2","2", secondPoll);
	}
	
	@Test
	public void LLPeek() {
		LinkedList<String> LL = new LinkedList<String>();
		LL.offer("1");
		LL.offer("2");
		assertEquals("First string is 1","1", LL.peek());
	}
	
	@Test
	public void LLIteratorTest_hasnext() {
		LinkedList<String> LL = new LinkedList<String>();
		LL.offer("1");
		LL.offer("2");
		Iterator<String> it = LL.iterator();
		assertEquals("LL has a next element",true, it.hasNext());
	}
	
	@Test
	public void LLIteratorTest_next() {
		LinkedList<String> LL = new LinkedList<String>();
		LL.offer("1");
		LL.offer("2");
		Iterator<String> it = LL.iterator();
		String test = it.next();
		assertEquals("LL's next elemnt is 1","1", test);
		test = it.next();
		assertEquals("LL's next elemnt is 2","2", test);
	}
}
