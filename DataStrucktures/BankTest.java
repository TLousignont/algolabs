import static org.junit.Assert.*;

import org.junit.Test;

import edu.neumont.util.Client;


public class BankTest {
	
	@Test
	public void BankConstrution() {
		Bank testBank = new Bank(2);
		assertEquals("There are 2 tellers",2, testBank.Tellers.length);
	}
	
	@Test
	public void BankAddCustomer() {
		Bank testBank = new Bank(2);
		Client client = new Client(10);
		testBank.addCustomer(client);
		assertEquals("The first customers wait time is 10.",10, testBank.clients.peek().getExpectedServiceTime());
	}

	@Test
	public void BankgetClientWaitTimeOne() {
		Bank testBank = new Bank(2);
		Client client1 = new Client(10);
		testBank.addCustomer(client1);
		double waitTime = testBank.getClientWaitTime(client1);
		double expected = 0.0;
		assertEquals("Client 1's wait time is 0",expected, waitTime,0.1);
	}
	
	@Test
	public void BankgetClientWaitTimeTwo() {
		Bank testBank = new Bank(2);
		Client client1 = new Client(10);
		Client client2 = new Client(10);
		testBank.addCustomer(client1);
		testBank.addCustomer(client2);
		double waitTime = testBank.getClientWaitTime(client2);
		double expected = 0.0;
		assertEquals("Client 2's wait time is 0",expected, waitTime,0.1);
	}
	
	@Test
	public void BankgetClientWaitTimeThree() {
		Bank testBank = new Bank(2);
		Client client1 = new Client(10);
		Client client2 = new Client(10);
		Client client3 = new Client(10);
		testBank.addCustomer(client1);
		testBank.addCustomer(client2);
		testBank.addCustomer(client3);
		double waitTime = testBank.getClientWaitTime(client3);
		double expected = 10.0;
		assertEquals("Client 3's wait time is 10",expected, waitTime,0.1);
	}
	
	@Test
	public void BankgetAvreageClientWaitTime() {
		Bank testBank = new Bank(2);
		Client client1 = new Client(30);//0
		Client client2 = new Client(30);//0
		Client client3 = new Client(30);//20
		testBank.addCustomer(client1);
		testBank.addCustomer(client2);
		testBank.addCustomer(client3);
		double aveTime = testBank.getAverageClientWaitTime();
		System.out.println("Test avrage: " +aveTime);
		double expected = 10;
		assertEquals("Avrage wait time is 10",expected, aveTime,0.1);
	}
	
	@Test
	public void BankAdvanceMinuteWaitTimeChangeThreePersonLine() {
		Bank testBank = new Bank(2);
		Client client1 = new Client(10);//0
		Client client2 = new Client(10);//10
		Client client3 = new Client(10);//20
		testBank.addCustomer(client1);
		testBank.addCustomer(client2);
		testBank.addCustomer(client3);
		testBank.advanceMinute();
		double waitTime = testBank.getClientWaitTime(client3);
		double expected = 10.0;
		assertEquals("Client 3's wait time is 10",expected, waitTime,0.1);
	}
	
	@Test
	public void BankAdvanceMinuteWaitTimeChangeFourPersonLine() {
		Bank testBank = new Bank(2);
		Client client1 = new Client(10);//0
		Client client2 = new Client(10);//10
		Client client3 = new Client(10);//20
		Client client4 = new Client(10);//20
		testBank.addCustomer(client1);
		testBank.addCustomer(client2);
		testBank.addCustomer(client3);
		testBank.addCustomer(client4);
		testBank.advanceMinute();
		double waitTime = testBank.getClientWaitTime(client4);
		double expected = 10.0;
		assertEquals("Client 4's wait time is 10",expected, waitTime,0.1);
	}
	
	@Test
	public void BankAdvanceMinuteWaitTimeChangeFivePersonLine() {
		Bank testBank = new Bank(2);
		Client client1 = new Client(10);//0
		Client client2 = new Client(10);//10
		Client client3 = new Client(10);//20
		Client client4 = new Client(10);//20
		Client client5 = new Client(10);//20
		testBank.addCustomer(client1);
		testBank.addCustomer(client2);
		testBank.addCustomer(client3);
		testBank.addCustomer(client4);
		testBank.addCustomer(client5);
		testBank.advanceMinute();
		double waitTime = testBank.getClientWaitTime(client5);
		System.out.println(waitTime);
		double expected = 20.0;
		assertEquals("Client 5's wait time is 20",expected, waitTime,0.1);
	}
	
	@Test
	public void BankAdvanceMinuteNewClientAtTeller() {
		Bank testBank = new Bank(2);
		Client client1 = new Client(10);//0
		Client client2 = new Client(10);//10
		Client client3 = new Client(10);//20
		testBank.addCustomer(client1);
		testBank.addCustomer(client2);
		testBank.addCustomer(client3);
		for(int i = 0; i < 11; i++) {
			testBank.advanceMinute();
		}
		testBank.advanceMinute();
		assertEquals("Client 3 has moved to a teller",client3, testBank.Tellers[0]);
	}
}
