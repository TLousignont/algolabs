import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;


public class BfsGraphTraversal {
	public List<List<Integer>> traverse(Graph g){
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		for(int i = 0; i < g.vectorCount(); i++) {
			if(g.getMark(i) == 0){
				List<Integer> temp = new ArrayList<Integer>();
				BFS(g,i, temp);
				list.add(temp);
			}
		}
		return list;
	}
	
	private void BFS(Graph g, int v, List<Integer> toEdit) {
		Queue<Integer> Q = new PriorityQueue<Integer>();
		g.setMark(v, 1);
		toEdit.add(v);
		Q.add(v);
		while(!Q.isEmpty()){
			int temp = Q.remove();
			int child = 0;
			for(int w = g.getFirstNeighbor(temp); w < g.vectorCount(); w = g.nextNeighbor(temp, w)){
				if(g.getMark(w) == 0){
					g.setMark(w, 1);
					Q.add(w);
					toEdit.add(w);
				}
			}
		}
		
	}
}
