
public class Graph {
	private int[][] matrix;
	private int[] marks;
	private int numEdges;
	public Graph(int n){
		matrix = new int[n][n];
		marks = new int[n];
	}
	
	public void resetMarks() {
		for(int i = 0; i < marks.length; i++){
			marks[i] = 0;
		}
	}
	
	public int vectorCount() {
		return marks.length;
	}
	
	//first neighbor
	public int getFirstNeighbor(int v){
		for(int i = 0; i < vectorCount(); i++){
			if(matrix[v][i] != 0) {
				return i;
			}
		}
		return vectorCount();
	}
	
	//next neighbor
	public int nextNeighbor(int v, int neighbor) {
		for(int i = neighbor+1; i < vectorCount(); i++){
			if(matrix[v][i] != 0) {
				return i;
			}
		}
		return vectorCount();
	}
	
	public void addEdge(int vertex, int neighbor, int weight) {
		matrix[vertex][neighbor] = weight;
		matrix[neighbor][vertex] = weight;
		numEdges++;
	}
	
	public void removeEdge(int vertex, int neighbor) {
		matrix[vertex][neighbor] = 0;
		matrix[neighbor][vertex] = 0;
		numEdges--;
	}
	
	public boolean isEdge(int vertex, int neighbor) {
		return matrix[vertex][neighbor] != 0;
	}
	
	public int getMark(int vertex) {
		return marks[vertex];
	}
	
	public void setMark(int vertex, int mark){
		marks[vertex] = mark;
	}
	
	public int ecount(){
		return numEdges;
		// returns the number of edges in the graph
	}
	
	public int degree(int v){
		int edgeCount = 0;
		for(int i = 0; i < vectorCount(); i++){
			if(matrix[v][i] != 0) {
				edgeCount++;
			}
		}
		return edgeCount;
	}
}
