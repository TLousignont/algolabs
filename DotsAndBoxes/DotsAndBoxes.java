import java.util.Iterator;
import java.util.List;

public class DotsAndBoxes {
	private int[] scores = new int[2];
	private int numColumns;
	private int numRows;
	private int numNodes;
	private Graph g;

	public DotsAndBoxes(int rows, int columns) {
		numColumns = columns + 1;
		numRows = rows + 1;
		numNodes = numRows * numColumns;
		g = new Graph(numNodes);
		int row = 0;
		int column = 0;
		for (int i = 0; i < numNodes; i++) {
			if (!(row == 0 || row == rows || column == 0 || column == columns)) {
				g.addEdge(i, i - numColumns, 1);
				g.addEdge(i, i - 1, 1);
				g.addEdge(i, i + 1, 1);
				g.addEdge(i, i + numColumns, 1);
			}
			if (column == columns) {
				column = 0;
				row++;
			} else {
				column++;
			}
		}
	}

	public void printPlayGrouond() {
		int row = 0;
		int column = 0;
		String rowDisplay = "";
		String bottomConnection = "";
		for (int i = 0; i < numNodes; i++) {
			if (column - 1 >= 0) {
				if (g.isEdge(i, i - 1)) {
					rowDisplay += "-";
				} else {
					rowDisplay += " ";
				}
			} else {
				rowDisplay += " ";
			}

			if (!(column < 0)) {
				if ((row + 1 < numRows)) {
					bottomConnection += " ";
				}
			}

			rowDisplay += "o";

			if (row + 1 < numRows) {
				if (g.isEdge(i, i + numColumns)) {
					bottomConnection += "|";
				} else {
					bottomConnection += " ";
				}
			}

			if (column == numColumns - 1) {
				column = 0;
				System.out.println(rowDisplay);
				System.out.println(bottomConnection);
				rowDisplay = "";
				bottomConnection = "";
				row++;
			} else {
				column++;
			}
		}
	}

	public int drawLine(int player, int x1, int y1, int x2, int y2) {
		int pointsGained = 0;
		if (x1 == x2 && (y1 - 1 == y2)) {
			int column = y1;
			int topCoin = (column + (x1 * numColumns));
			int bottomCoin = (column + ((x1 + 1) * numColumns));
			g.removeEdge(topCoin, bottomCoin);
			if (isBoxCompleted(topCoin)) {
				System.out.println("Top Node : " + topCoin + " Got the point");
				pointsGained++;
				scores[player]++;
			}
			if (isBoxCompleted(bottomCoin)) {
				System.out.println("Bottom Node : " + bottomCoin + " Got the point");
				pointsGained++;
				scores[player]++;
			}
		} 
		else if (x1 == x2 && (y1 + 1 == y2)) {
			// right
			int column = y1 + 1;
			int topCoin = (column + (x1 * numColumns));
			int bottomCoin = (column + ((x1 + 1) * numColumns));
			g.removeEdge(topCoin, bottomCoin);
			if (isBoxCompleted(topCoin)) {
				System.out.println("Top Node : " + topCoin + " Got the point");

				pointsGained++;
				scores[player]++;
			}
			if (isBoxCompleted(bottomCoin)) {
				System.out.println("Bottom Node : " + bottomCoin + " Got the point");
				pointsGained++;
				scores[player]++;
			}
		}

		else if (y1 == y2 && (x1 - 1 == x2)) {
			// top
			int row = x1;
			int leftCoin = (y1 + (row * numColumns));
			int rightCoin = leftCoin + 1;
			g.removeEdge(leftCoin, rightCoin);
			if (isBoxCompleted(leftCoin)) {
				System.out.println("Left Node : " + leftCoin + " Got the point");
				pointsGained++;
				scores[player]++;
			}
			if (isBoxCompleted(rightCoin)) {
				System.out.println("Right Node : " + rightCoin + " Got the point");
				pointsGained++;
				scores[player]++;
			}
		}

		else if (y1 == y2 && (x1 + 1 == x2)) {
			// bottom
			int row = x1 + 1;
			int leftCoin = (y1 + (row * numColumns));
			int rightCoin = leftCoin + 1;
			g.removeEdge(leftCoin, rightCoin);
			if (isBoxCompleted(leftCoin)) {
				pointsGained++;
				scores[player]++;
			}
			if (isBoxCompleted(rightCoin)) {
				pointsGained++;
				scores[player]++;
			}
		}
		return pointsGained;
		// draws a line from (x1, y1) to (x2, y2) (0,0) is in the upper-left
		// corner, returning how many points were earned, if any
	}

	private boolean isBoxCompleted(int coin) {
		boolean boxCompleted = false;
		if (!isCoinOnEdge(coin)) {
			boxCompleted = g.degree(coin) == 0;
		}
		return boxCompleted;
	}

	private boolean isCoinOnEdge(int coin) {
		return (coin) % numColumns == 0
				|| (coin) % numColumns == numColumns - 1
				|| coin < numColumns || coin > (numColumns * (numRows - 1));
	}

	int score(int player) {
		return scores[player];
	}

	boolean areMovesLeft() {
		return !(g.ecount() == 0);
		// returns whether or not there are any lines to be drawn
	}

	int countDoubleCrosses() {
		g.resetMarks();
		int numDoubleCrosses = 0;
		DfsGraphTraversal dfs = new DfsGraphTraversal();
		List<List<Integer>> check = dfs.traverse(g);
		Iterator it = check.iterator();
		while(it.hasNext()){
			List<Integer> innerList = (List<Integer>) it.next();
			if(innerList.size() == 2) {
				if(!(isCoinOnEdge(innerList.get(0))) && !(isCoinOnEdge(innerList.get(1)))){
					numDoubleCrosses++;
				}
			}
		}
		return numDoubleCrosses;
	}

	int countCycles() {
		g.resetMarks();
		int numCycles = 0;
		DfsGraphTraversal dfs = new DfsGraphTraversal();
		List<List<Integer>> check = dfs.traverse(g);
		Iterator it = check.iterator();
		while(it.hasNext()){
			boolean isCycle = true;
			List<Integer> innerList = (List<Integer>) it.next();
			for(int i = 0; i < innerList.size(); i++){
				int temp = innerList.get(i);
				if(!(g.degree(temp) == 2) || isCoinOnEdge(temp)){
					isCycle = false;
				}
			}
			if(isCycle){
				numCycles++;
			}
		}
		return numCycles;
	}

	int countOpenChains() {
		g.resetMarks();
		int numOpenchains = 0;
		DfsGraphTraversal dfs = new DfsGraphTraversal();
		List<List<Integer>> check = dfs.traverse(g);
		Iterator it = check.iterator();
		while(it.hasNext()){
			List<Integer> innerList = (List<Integer>) it.next();
			//boolean isOpenChain = true;
			for(int i = 0; i < innerList.size(); i++){
				boolean isOpenChain = true;

				int temp = innerList.get(i);
				
				if(g.degree(temp) >= 3 || (g.degree(temp) == 1 && isCoinOnEdge(temp))){
					boolean checkingChain = true;
					int partsCount = 0;
					int index = i+1;
					while(checkingChain && index < innerList.size()){
						int link = innerList.get(index);
						if(g.degree(link) == 2){
							partsCount ++;
						}
						else {
							checkingChain = false;
						}
						index ++;
					}
					if(!(partsCount > 2)){
						isOpenChain = false;
					}
				}
				else {
					isOpenChain = false;
				}
				if(isOpenChain){
					numOpenchains ++;
				}
			}
		}
		return numOpenchains;
		// returns the number of open chains on the board
	}

}
