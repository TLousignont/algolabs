import java.util.ArrayList;
import java.util.List;


public class DfsGraphTraversal {
	public List<List<Integer>> traverse(Graph g){
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		for(int i = 0; i < g.vectorCount(); i++) {
			if(g.getMark(i) == 0){
				List<Integer> temp = new ArrayList<Integer>();
				DFS(g,i, temp);
				list.add(temp);
			}
		}
		return list;
	}

	
	private void DFS(Graph g, int v, List<Integer> toEdit) {
		g.setMark(v, 1);
		for(int w = g.getFirstNeighbor(v); w < g.vectorCount(); w = g.nextNeighbor(v, w)){
			if(g.getMark(w) == 0){
				DFS(g, w, toEdit);
			}
		}
		toEdit.add(v);
	}
}
