import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.StringEncoder;

import edu.neumont.nlp.DecodingDictionary;


public class ExhaustiveDecoder {
	DecodingDictionary d;
	public ExhaustiveDecoder() {
		// TODO Auto-generated constructor stub
	}

	public ExhaustiveDecoder(DecodingDictionary inputD) {
		d = inputD;
	}
	
	public List<String> decode(String message) {
		List<String> Gueses = new ArrayList<String>();
		String phrase = "";
		String lastWord = "";
		decodeHelper(Gueses,phrase,message,lastWord, 0);
		return Gueses;
	}
	
	
	public void decodeHelper(List<String> phrases, String phrase, String Message, String LastWord, float score){
		
		//win
		//Add to the list if the string code is empty
		if(Message.isEmpty()){
			phrases.add(phrase);
			return;
		}
		
		//fail
		//if there is unused data in the message string that caonnot be made into a word
		
		//recall
		//For each word in the input list create a list of possible words that could follow.
			//Limit by its frequeancy number
		//First List of words.
		if(phrase.isEmpty() && LastWord.isEmpty()){
			for(int i = 0; i < Message.length(); i++) {
				String codeSnip = Message.substring(0, i+1);
				Set<String> wordsToAdd  = d.getWordsForCode(codeSnip);
				if(wordsToAdd != null){
					Iterator<String> wI = wordsToAdd.iterator();
					while(wI.hasNext()){
						String word =  wI.next();
						String input = phrase + word;
						String cutMessage = Message.substring(i+1, Message.length());
						decodeHelper(phrases,input, cutMessage, word, 1);
					}
				}
			}
		}
		//Second word and onwards.
		else {
			for(int i = 0; i < Message.length(); i++)
			{
				String codeSnip = Message.substring(0, i+1);
				Set<String> wordsToAdd  = d.getWordsForCode(codeSnip);
				if(wordsToAdd != null){
					Iterator<String> wI = wordsToAdd.iterator();
					while(wI.hasNext()){
						String word =  wI.next();
						String input = phrase +" "+ word;
						String cutMessage = Message.substring(i+1, Message.length());
						int frequency = d.frequencyOfFollowingWord(LastWord, word);
						float calScore = (float)frequency/10000.0f * (float)score;
						if(frequency > 50 && calScore > 0.000000001f){
							decodeHelper(phrases,input, cutMessage, word, calScore);
						}
					}
				}
			}
		}
	}	
}
