import java.util.Collections;
import java.util.List;


public class SelectionSorter<T extends Comparable> implements Sorter<T> {

	public SelectionSorter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void sort(List<T> unsorted) {
		// TODO Auto-generated method stub
		int min;
		for(int i = 0; i < unsorted.size()-1; i++){
			min = i;
			for(int j = i; j < unsorted.size(); j++){
				if(unsorted.get(j).compareTo(unsorted.get(min)) < 0){
					Collections.swap(unsorted, j, min);
				}
			}
		}
	}
}
