import java.util.List;

import edu.neumont.csc250.lab4.Book;
import edu.neumont.csc250.lab4.Bookcase;

public class DynamicShelver implements Shelver {

	public DynamicShelver() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void shelveBooks(Bookcase bookcase, List<Book> books) {
		// TODO Auto-generated method stub
		int n = books.size();
		int M = bookcase.getShelfWidth();
		int[][] extras = new int[n + 1][n + 1];
		int[][] lc = new int[n + 1][n + 1];
		int[] c = new int[n + 1];
		int[] p = new int[n + 1];
		int i, j;

		//CalcExtraSpace
		for (i = 1; i <= n; i++) {
			extras[i][i] = M - books.get(i - 1).getWidth();
			for (j = i + 1; j <= n; j++)
				extras[i][j] = extras[i][j - 1] - books.get(j - 1).getWidth()- 1;
		}
		
		//CalcLineCost
		//-If below zero throw it away
		//-If exact length = 0
		//-else calc score
		for (i = 1; i <= n; i++) {
			for (j = i; j <= n; j++) {
				if (extras[i][j] < 0)
					lc[i][j] = Integer.MAX_VALUE;
				else if (j == n && extras[i][j] >= 0)
					lc[i][j] = 0;
				else
					lc[i][j] = extras[i][j] * extras[i][j];
			}
		}
		
		c[0] = 0;
		for (j = 1; j <= n; j++) {
			c[j] = Integer.MAX_VALUE;
			for (i = 1; i <= j; i++) {
				if (c[i - 1] != Integer.MAX_VALUE
						&& lc[i][j] != Integer.MAX_VALUE
						&& (c[i - 1] + lc[i][j] < c[j])) {
					c[j] = c[i - 1] + lc[i][j];
					p[j] = i;
				}
			}
		}
		printSolution(p, n, bookcase, books);
	}
	
	int printSolution (int p[], int n, Bookcase bookcase, List<Book> books)
	{
	    int k;
	    if (p[n] == 1)
	        k = 1;
	    else
	        k = printSolution (p, p[n]-1, bookcase, books) + 1;
	    System.out.println("Line number " + k +": From word no." + p[n] + " to " + n);
	    if(k-1 < bookcase.getNumberOfShelves()){
	    	System.out.println("You are on shelf: " + k);
	    	System.out.println("p[n]: " + p[n]);
	    	System.out.println("n: " + n);
	    	for(int i = p[n]-1; i < n+1; i++){
		    	System.out.println("adding book: " + i);
		    	bookcase.addBook(k-1, books.get(i));
		    }
	    }
	    return k;
	}
}
