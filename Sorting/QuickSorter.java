import java.util.Collections;
import java.util.List;


public class QuickSorter<T extends Comparable> implements Sorter<T> {

	public QuickSorter() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public void sort(List<T> unsorted) {
		quickSort(unsorted,0,unsorted.size()-1);
	}
	
	private void quickSort(List<T> A, int left, int Right){
		int pivotIndex = findpivot(A, left, Right);
		Collections.swap(A, pivotIndex, Right);
		int k = partition(A, left - 1, Right, A.get(Right));
		Collections.swap(A, k, Right);
		if((k-left) > 1) quickSort(A, left, k-1);
		if((Right-k) > 1) quickSort(A, k+1, Right);
	}
	
	private int partition(List<T> a, int left, int right, T pivot) {
		do{
			while(a.get(++left).compareTo(pivot)<0);
			while((right!=0)&&(a.get(--right).compareTo(pivot)>0));
			Collections.swap(a, left, right);
		}while(left<right);
		Collections.swap(a, left, right);
		return left;
		
	}
	
	private int findpivot(List<T> a, int i, int j) {
		return(i+j)/2;
	}
 
}
