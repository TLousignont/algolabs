import java.util.List;

public interface Sorter<T extends Comparable> {
	/**
	* Sorts a list in-place
	*/
	public void sort(List<T> unsorted);
}
