import java.util.ArrayList;
import java.util.List;


public class MergeSorter<T extends Comparable> implements Sorter<T> {

	public MergeSorter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void sort(List<T> unsorted) {
		List<T> sorted = new ArrayList<T>( mergeSortImp(unsorted));
		unsorted.clear();
		unsorted.addAll( sorted);
		//mergeSort(unsorted,unsorted,0, unsorted.size()-1);
	}
	
	private List<T> mergeSortImp(List<T> items){
		if(items.size() <= 1){
			return items;
		}
		
		List<T> left = new ArrayList<T>();
		List<T> right = new ArrayList<T>();
		int middle = items.size()/2;
		for(int i = 0; i < middle; i ++){
			left.add(items.get(i));
		}
		for(int i = middle; i < items.size(); i ++){
			right.add(items.get(i));
		}
		left = mergeSortImp(left);
		right = mergeSortImp(right);
		return merge(left,right);
	}
	
	private List<T> merge(List<T> left, List<T> right) {
		List<T> result = new ArrayList<T>();
		while(left.size() > 0 || right.size() > 0){
			if(left.size() > 0 && right.size() > 0){
				if(left.get(0).compareTo(right.get(0)) < 0){
					result.add(left.get(0));
					left.remove(0);
				}
				else {
					result.add(right.get(0));
					right.remove(0);
				}
			}
			else if(left.size() > 0){
				result.add(left.get(0));
				left.remove(0);
			}
			else if(right.size() > 0){
				result.add(right.get(0));
				right.remove(0);
			}
		}
		return result;
	}

	private void mergeSort(List<T> A, List<T> temp, int left, int right){
		int mid = (left+right)/2;
		if(left == right){
			return;
		}
		//ArrayList<T> newList = new ArrayList<T>(A);
		//ArrayList<T> tempList = new ArrayList<T>(temp);
		
		mergeSort(A, temp, left, mid);
		mergeSort(A, temp, mid+1, right);
		for(int i = left; i <= right; i++){
			temp.set(i, A.get(i));
		}
		int i1 = left;
		int i2 = mid+1;
		for(int current = left; current <= right; current++){
			if(i1 == mid+1){
				A.set(current, temp.get(i2++));
			}
			else if( i2 > right){
				A.set(current, temp.get(i1++));
			}
			else if(temp.get(i1).compareTo(temp.get(i2)) < 0){
				A.set(current, temp.get(i1++));
			}
			else {
				A.set(current, temp.get(i2++));
			}
		}
	}
}
