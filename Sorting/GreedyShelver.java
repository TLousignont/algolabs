import java.util.List;
import edu.neumont.csc250.lab4.Book;
import edu.neumont.csc250.lab4.Bookcase;


public class GreedyShelver implements Shelver {

	public GreedyShelver() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void shelveBooks(Bookcase bookcase, List<Book> books) {
		// TODO Auto-generated method stub
		SelectionSorter<Book> bookSorter = new SelectionSorter<Book>();
		bookSorter.sort(books);
		int shelfeWidth = bookcase.getShelfWidth();
		int numShelf = bookcase.getNumberOfShelves();
		int currentShelf = 0;
		int useWidth = 0;
		for(int i = 0; i < books.size(); i++){
			if(currentShelf < numShelf){
				int nextBookwidth = (books.get(i).getWidth() + useWidth);
				if(nextBookwidth <= shelfeWidth){
					bookcase.addBook(currentShelf, books.get(i));
					useWidth += books.get(i).getWidth(); 
				}
				else {
					currentShelf++;
					useWidth = 0;
					nextBookwidth = books.get(i).getWidth() + useWidth;
					if((books.get(i).getWidth() + useWidth) <= shelfeWidth){
						bookcase.addBook(currentShelf, books.get(i));
						useWidth += books.get(i).getWidth(); 
					}
				}
			}
		}
	}

}
