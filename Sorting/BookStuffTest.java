import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import edu.neumont.csc250.lab4.Book;
import edu.neumont.csc250.lab4.Bookcase;



public class BookStuffTest {

	@Test
	public void greedyShelverSimpleShelving() {
		GreedyShelver gs = new GreedyShelver();
		Bookcase bookcase = new Bookcase(2, 4);
		
		List<Book> books = new ArrayList<Book>();
		//Title, width, height
		books.add(new Book("test1", 2, 1));
		books.add(new Book("test2", 2, 2));
		
		gs.shelveBooks(bookcase, books);
		assertEquals("Shelf 1 should be full",0, bookcase.getBookshelf(0).getSpaceLeft());
	}
	
	@Test
	public void greedyShelver4books() {
		GreedyShelver gs = new GreedyShelver();
		Bookcase bookcase = new Bookcase(3, 4);
		
		List<Book> books = new ArrayList<Book>();
		//Title, width, height
		books.add(new Book("test1", 2, 5));
		books.add(new Book("test2", 2, 2));
		books.add(new Book("test3", 1, 1));
		books.add(new Book("test4", 3, 2));
		
		gs.shelveBooks(bookcase, books);
		assertEquals("Shelf 1 should have 1 empty",1, bookcase.getBookshelf(0).getSpaceLeft());
		assertEquals("Shelf 2 should have 1 empty",1, bookcase.getBookshelf(1).getSpaceLeft());
		assertEquals("Shelf 3 should have 2 empty",2, bookcase.getBookshelf(2).getSpaceLeft());
	}
	
	@Test
	public void greedyShelver10books() {
		GreedyShelver gs = new GreedyShelver();
		Bookcase bookcase = new Bookcase(7, 6);
		
		List<Book> books = new ArrayList<Book>();
		//Title, width, height
		books.add(new Book("test1", 2, 5));//
		books.add(new Book("test2", 2, 2));//
		books.add(new Book("test3", 1, 1));//
		books.add(new Book("test4", 3, 2));//
		books.add(new Book("test5", 5, 5));//
		books.add(new Book("test6", 2, 2));//
		books.add(new Book("test7", 4, 1));//
		books.add(new Book("test8", 3, 2));//
		books.add(new Book("test9", 3, 5));//
		books.add(new Book("test10", 6, 1));//
		//s1 test3| test7 		left over is 1
		//s2 test10				left over is 0
		//s3 test2|test4		left over is 1
		//s4 test6|test8		left over is 1
		//s5 test1|				left over is 4
		//s6 test5				left over is 1
		//s7 test9 				left over is 3
		gs.shelveBooks(bookcase, books);
		assertEquals("Shelf 1 should have 1 empty",1, bookcase.getBookshelf(0).getSpaceLeft());
		assertEquals("Shelf 2 should have 0 empty",0, bookcase.getBookshelf(1).getSpaceLeft());
		assertEquals("Shelf 3 should have 2 empty",2, bookcase.getBookshelf(2).getSpaceLeft());
		assertEquals("Shelf 4 should have 0 empty",0, bookcase.getBookshelf(3).getSpaceLeft());
		assertEquals("Shelf 5 should have 1 empty",1, bookcase.getBookshelf(4).getSpaceLeft());
		assertEquals("Shelf 6 should have 1 empty",1, bookcase.getBookshelf(5).getSpaceLeft());
		assertEquals("Shelf 7 should have 6 empty",6, bookcase.getBookshelf(6).getSpaceLeft());
	}
	
	
	@Test
	public void dynamicShelver10books() {
		DynamicShelver gs = new DynamicShelver();
		Bookcase bookcase = new Bookcase(7, 6);
		
		List<Book> books = new ArrayList<Book>();
		//Title, width, height
		books.add(new Book("test1", 2, 5));//
		books.add(new Book("test2", 2, 2));//
		books.add(new Book("test3", 1, 1));//
		books.add(new Book("test4", 3, 2));//
		books.add(new Book("test5", 5, 5));//
		books.add(new Book("test6", 2, 2));//
		books.add(new Book("test7", 4, 1));//
		books.add(new Book("test8", 3, 2));//
		books.add(new Book("test9", 3, 5));//
		books.add(new Book("test10", 6, 1));//
		//s1 test3| test7 		left over is 1
		//s2 test10				left over is 0
		//s3 test2|test4		left over is 1
		//s4 test6|test8		left over is 1
		//s5 test1|				left over is 4
		//s6 test5				left over is 1
		//s7 test9 				left over is 3
		gs.shelveBooks(bookcase, books);
		System.out.println(bookcase.toString());
	}
}
