import java.util.List;
import edu.neumont.csc250.lab4.Book;
import edu.neumont.csc250.lab4.Bookcase;

public interface Shelver {
	/**
	* Takes a list of books and places them onto an empty bookcase.
	*/
	public void shelveBooks (Bookcase bookcase, List<Book> books);
}
