import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


public class SortingTests {

	
	@Test
	public void selectionTest() {
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(1);//
		numbers.add(11);
		numbers.add(6);//
		numbers.add(4);//
		numbers.add(8);
		numbers.add(9);
		numbers.add(5);//
		numbers.add(3);//
		numbers.add(2);//
				
		SelectionSorter<Integer> sorter = new SelectionSorter<Integer>();
		sorter.sort(numbers);
		List<Integer> correct = new ArrayList<Integer>();
		correct.add(1);
		correct.add(2);
		correct.add(3);
		correct.add(4);
		correct.add(5);
		correct.add(6);
		correct.add(8);
		correct.add(9);
		correct.add(11);
		assertEquals("List numbers should be sorted to list correct.",correct, numbers);
		
	}
	
	@Test
	public void quicksortTest() {
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(1);//
		numbers.add(11);
		numbers.add(6);//
		numbers.add(4);//
		numbers.add(8);
		numbers.add(9);
		numbers.add(5);//
		numbers.add(3);//
		numbers.add(2);//
				
		QuickSorter<Integer> sorter = new QuickSorter<Integer>();
		sorter.sort(numbers);
		List<Integer> correct = new ArrayList<Integer>();
		correct.add(1);
		correct.add(2);
		correct.add(3);
		correct.add(4);
		correct.add(5);
		correct.add(6);
		correct.add(8);
		correct.add(9);
		correct.add(11);
		assertEquals("List numbers should be sorted to list correct.",correct, numbers);
		
	}
	
	@Test
	public void mergTest() {
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(1);//1
		numbers.add(11);//2
		numbers.add(6);//3
		numbers.add(4);//4
		numbers.add(8);//5
		numbers.add(9);//6
		numbers.add(5);//7
		numbers.add(3);//8
		numbers.add(2);//9
				
		MergeSorter<Integer> sorter = new MergeSorter<Integer>();
		sorter.sort(numbers);
		List<Integer> correct = new ArrayList<Integer>();
		correct.add(1);//1
		correct.add(2);//2
		correct.add(3);//3
		correct.add(4);//4
		correct.add(5);//5
		correct.add(6);//6
		correct.add(8);//7
		correct.add(9);//8
		correct.add(11);//9
		assertEquals("List numbers should be sorted to list correct.",correct, numbers);
		
	}

}
