import java.awt.Color;

import edu.neumont.ui.Picture;


public class SeamCarver {
	Picture image;
	int width;
	int height;
	int[][] powers;
		public void MySeamCarver(Picture pic){
			image = pic;
			width = pic.width();
			height = pic.height();
			powers = new int[width][height];
			calcPicturesPower();
		}
		
		private void calcPicturesPower(){
			for(int i = 0; i < width; i ++){
				for(int  j = 0; j < height; j ++){
					calcPower(i,j);
				}
			}
		}
		
		private void calcPower(int x, int y){
			Color right,left,up,down;
			if(x+1 >= width){
				 right = image.get(0,y);
			}
			else {
				 right = image.get(x+1,y);
			}
			if(x-1 < 0){
				 left = image.get(width-1,y);
			}
			else {
				 left = image.get(x-1,y);
			}
			if(y+1 >= height){
				 up = image.get(x,0);
			}
			else {
				 up = image.get(x,y+1);
			}
			if(y-1 < 0){
				 down = image.get(x,height-1);
			}
			else {
				 down = image.get(x,y-1);
			}
			int rx = left.getRed() - right.getRed();
			int gx = left.getGreen() - right.getGreen();
			int bx = left.getBlue() - right.getBlue(); 
			int deltaX = (rx *rx) + (gx*gx) + (bx*bx);
			int ry = up.getRed() - down.getRed();
			int gy = up.getGreen() - down.getGreen();
			int by = up.getBlue() - down.getBlue(); 
			int deltaY = (ry *ry) + (gy*gy) + (by*by);
			
			powers[x][y] = deltaX + deltaY;
		}
		
		public Picture getPicture (){
			return image;
		}
		
		public int width(){
			return width;
			
		}
		
		public int height(){
			return height;
			
		}
		
		public double energy(int x, int y){
			if(x >= width || x < 0 || y < 0 || y >= height){
					throw new java.lang.IndexOutOfBoundsException();
			}
			return powers[x][y];
			// the energy of a pixel at (x,y)
		}

		public int[] findHorizontalSeam(){
			int totalPower = Integer.MAX_VALUE;
			int[] seam = new int[width];
			for(int i = 0; i < height; i++){
				int tempIndex = 0;
				int[] tempSeam = new int[width];
				tempSeam[tempIndex] = i;
				int tempPower = powers[0][i];
				int selectedHeight = i;
				for(int j = 1; j < width; j++){
					tempIndex++;
					int up,middel,down;
					up = (selectedHeight -1 < 0)?Integer.MAX_VALUE:powers[j][(selectedHeight-1)];
					middel = powers[j][selectedHeight];
					
					down = (selectedHeight +1 >= height)?Integer.MAX_VALUE:powers[j][selectedHeight +1];
					if(up < middel && up < down){
						tempPower += up;
						selectedHeight = selectedHeight -1;
					}
					else if(middel < down && middel < up){
						tempPower += middel;
					}
					else {
						tempPower += down;
						selectedHeight = selectedHeight + 1;
					}
					tempSeam[tempIndex] = selectedHeight;
				}
				if(tempPower < totalPower){
					totalPower = tempPower;
					seam = tempSeam;
				}
			}
			return seam;
		}
		
		public int[] findVerticalSeam(){
			int totalPower = Integer.MAX_VALUE;
			int[] seam = new int[height];
			for(int i = 0; i < width; i++){
				int tempIndex = 0;
				int[] tempSeam = new int[height];
				tempSeam[tempIndex] = i;
				int tempPower = powers[i][0];
				int selectedWidth = i;
				for(int j = 1; j < height; j++){
					tempIndex++;
					//System.out.println("Heights: " + height + " width: " + height +" Cycle: " + j + " Index: " + tempIndex + " SelectedWidth: " + selectedWidth);
					
					int up,middel,down;
					
					up = (selectedWidth -1 < 0)?Integer.MAX_VALUE:powers[(selectedWidth-1)][j];
					middel = powers[selectedWidth][j];
					down = (selectedWidth +1 >= width)?Integer.MAX_VALUE:powers[selectedWidth +1][j];
					
					if(up < middel && up < down){
						tempPower += up;
						selectedWidth = selectedWidth -1;
					}
					else if(middel < down && middel < up){
						tempPower += middel;
					}
					else {
						tempPower += down;
						selectedWidth = selectedWidth + 1;
					}
					tempSeam[tempIndex] = selectedWidth;
				}
				if(tempPower < totalPower){
					totalPower = tempPower;
					seam = tempSeam;
				}
			}
			return seam;
		}
		
		public void paintHorizontalSeam(int[] indices){
			for(int i = 0; i < indices.length; i++) {
				System.out.println("Indice: " + i + " Value: " + indices[i] );
				image.set(i, indices[i], new Color(255,0,0));
			}
		}
		
		public void removeHorizontalSeam(int[] indices){
			if(indices.length > width){
				throw new java.lang.IllegalArgumentException();
			}
			for(int i = 0; i < indices.length; i++) {
				for(int j = indices[i]; j < height; j++){
					if(j >= height || j < 0){
						throw new java.lang.IndexOutOfBoundsException();
					}
					if(j+1 < height){
						Color temp = image.get(i,j+1);
						image.set(i, j, temp);
					}
				}
			}
			height--;
			Picture pic = new Picture(width, height);
			for(int i = 0; i < width; i++){
				for(int j = 0; j < height; j++){
					pic.set(i, j, image.get(i, j));
				}
			}
			MySeamCarver(pic);
		}
		
		public void removeVerticalSeam(int[] indices){
			if(indices.length > height){
				throw new java.lang.IllegalArgumentException();
			}
			for(int i = 0; i < indices.length; i++) {
				for(int j = indices[i]; j < width; j++){
					if(j >= width || j < 0){
						throw new java.lang.IndexOutOfBoundsException();
					}
					if(j+1 < width){
						Color temp = image.get(j+1,i);
						image.set(j, i, temp);
					}
				}
			}
			width--;
			Picture pic = new Picture(width, height);
			for(int i = 0; i < width; i++){
				for(int j = 0; j < height; j++){
					pic.set(i, j, image.get(i, j));
				}
			}
			MySeamCarver(pic);
		}
}
