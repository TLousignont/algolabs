


public class EditDistanceCalculator {
	public int editDistance(String a, String b) {
		System.out.println(editDistanceFaclitate(a, b));
		return editDistanceFaclitate(a, b);
		//return editDistanceHelper( a, b, new int[a.length()+1][b.length()+1] );
	}
	
	private int editDistanceHelper(String a, String b, int[][] eds) {
		for ( int i = 1; i <= a.length(); i++ ) {
			eds[i][0] = b.length();
		}
		for ( int j = 1; j <= b.length(); j++ ) {
			eds[0][j] = a.length();
		}
		
		for ( int i = 1; i <= a.length(); i++ ) {
			for ( int j = 1;  j <= b.length(); j++ ) {
				int delete = eds[i-1][j] + 1;
				int insert = eds[i][j-1] + 1;
				int remove = eds[i-1][j-1] + (a.charAt(i-1) == b.charAt(j-1) ? 0 : 1);
				eds[i][j] = Math.min(delete, Math.min(insert, remove));
			}
		}
		
		return eds[a.length()][b.length()];
	}
	
	/**
	 * #2.  Add a recursive version here, which uses a memory function.  Change your internal implementation
	 * to use this version. (1 point)
	 */
	private int editDistanceFaclitate(String a, String b){
		int eds[][] = new int[a.length()+1][b.length()+1];
		for ( int i = 1; i <= a.length(); i++ ) {
			eds[i][0] = b.length();
		}
		for ( int j = 1; j <= b.length(); j++ ) {
			eds[0][j] = a.length();
		}
		for ( int i = 1; i <= a.length(); i++ ) {
			for ( int j = 1; j <= b.length(); j++ ) {
				eds[i][j] = -1;
			}
		}
		return editDistanceHelper(a, b,a.length(),b.length(), eds);
	}
	
	private int editDistanceHelper(String a, String b, int x, int y, int[][] eds) {
		if(eds[x][y] != -1)
		{
			return eds[x][y];
		}
		else 
		{
			int delete = editDistanceHelper(a,b, x-1, y, eds) + 1;
			int insert = editDistanceHelper(a,b, x, y-1, eds) + 1;
			int remove = editDistanceHelper(a,b, x-1, y-1, eds) + (a.charAt(x-1) == b.charAt(y-1) ? 0 : 1);
			eds[x][y] = Math.min(delete, Math.min(insert, remove));
		}
		return eds[x][y];
	}
}
