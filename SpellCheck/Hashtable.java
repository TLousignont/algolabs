

import java.util.Iterator;


public class Hashtable<K, V> {
	private Entry<K,V>[] values;
	private int size;
	
	public Hashtable(int initialCapacity) {
		values = (Entry<K,V>[])new Entry[initialCapacity];
	}
	
	/**
	 * #3b. Implement this (1 point)
	 * 
	 * @param key
	 * @param value
	 */
	public void put(K key, V value) {
		int hash = Math.abs(key.hashCode()) % values.length;
		if(key.hashCode() < 0  && hash != 0 ){
			hash = values.length - hash;
		}
		Entry<K, V> ent = values[hash];
		if(ent == null){
			values[hash] = new Entry<K, V>(key,value);
			size++;
		}
		else {
			while(ent.next != null && !ent.key.equals(key)){
				ent = ent.next;
			}
			if(ent.key.equals(key)){
				ent.data = value;
			} 
			else {
				ent.next = new Entry<K, V>(key,value);
				size++;
			}
		}
	}
	
	/**
	 * #3b. Implement this (1 point)
	 * @param key
	 * @return
	 */
	public V get(K key) {
		int hash = Math.abs(key.hashCode()) % values.length;
		if(key.hashCode() < 0 && hash != 0 ){
			hash = values.length - hash;
		}
		Entry<K, V> ent = values[hash];
		if(ent == null){
			return null;
		}
		else {
			while(ent.next != null && !ent.key.equals(key)){
				ent = ent.next;
			}
			if(ent.key.equals(key)){
				return ent.data;
			} 
			else {
				return null;
			}
		}
	}

	/**
	 * #3c.  Implement this. (1 point)
	 * 
	 * @param key
	 * @return
	 */
	public V remove(K key) {
		int hash = Math.abs(key.hashCode()) % values.length;
		if(key.hashCode() < 0 && hash != 0 ){
			hash = values.length - hash;
		}
		Entry<K, V> ent = values[hash];
		Entry<K, V> prev = null;
		if(ent == null){
			return null;
		}
		else {
			while(ent.next != null && !ent.key.equals(key)){
				prev = ent;
				ent = ent.next;
			}
			if(ent.key.equals(key)){
				if(prev == null){
					values[hash] = ent.next;
				}
				else {
					prev.next = ent.next;
				}
				size--;
				return ent.data;
			} 
			else {
				return null;
			}
		}
	}
	
	public int size() {
		return size;
	}
	
	public boolean containsKey(K key) {
		return this.get(key) != null; 
	}

	public Iterator<V> values() {
		return new Iterator<V>() {
			private int count = 0;
			private Entry<K, V> currentEntry;
			
			{
				while ( ( currentEntry = values[count] ) == null && count < values.length ) {
					count++;
				}
			}

			@Override
			public boolean hasNext() {
				return count < values.length;
			}

			@Override
			public V next() {
				V toReturn = currentEntry.data;
				currentEntry = currentEntry.next;
				while ( currentEntry == null && ++count < values.length && (currentEntry = values[count]) == null );
				return toReturn;
			}

			@Override
			public void remove() {
			}
			
		};
	}
	
	private static class Entry<K, V> {
		private K key;
		private V data;
		private Entry<K,V> next;
		
		public Entry(K key, V data) {
			this.key = key;
			this.data = data;
		}
		
		public String toString() {
			return "{" + key + "=" + data + "}";
		}
	}
}