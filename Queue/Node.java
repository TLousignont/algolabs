
public class Node<T extends Comparable<T>>{

	private T data;
	private Node<T> left;
	private Node<T> right;
	
	public Node (T head){
		data = head;
	}
	
	public T getValue(){
		return data;
	}
	
	public void setLeft(Node value){
		left = value;
	}
	
	public Node getLeft(){
		return left;
	}
	
	public void setRight(Node value){
		right = value;
	}

	public Node getRight(){
		return right;
	}
	
	public boolean isLeaf(){
		boolean leaf = false;
		if(left == null && right == null){
			leaf = true;
		}
		return leaf;
	}
}
