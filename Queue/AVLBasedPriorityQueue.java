
public class AVLBasedPriorityQueue<T extends Comparable<T>> {

	Node<T> head;
	
	public AVLBasedPriorityQueue(T data){
		Node<T> temp = new Node<T>(data);
		head = temp ;
	}
	
	public Node<T> getHead(){
		return head;
	}
	
	public boolean offer(T data){
		Node<T> dataNode = new Node<T>(data);
		return offerHelp(head, dataNode, null);
	}
	
	private boolean offerHelp(Node<T> root, Node<T> newNode, Node<T> parent) {
		boolean larger = newNode.getValue().compareTo(root.getValue()) == 1;
		if(root.isLeaf()){
			if(larger){
				root.setRight(newNode);
				return true;
			}
			else {
				root.setLeft(newNode);
				return true;
			}
		}
		else {
			boolean temp = false;
			if(larger){
				temp = offerHelp(root.getRight(), newNode, root);
				balanceTree(root, parent);
			}
			else {
				temp = offerHelp(root.getLeft(), newNode, root);
				balanceTree(root, parent);
			}
			return temp;
		}
	}
	
	public <T>T peek(){
		return (T) head.getValue();
	}
	
	public <T>T poll(){
		T temp = (T)head.getValue();
		if(head.getLeft() != null) {
			head = getRightMost(head.getLeft());
		} else if( head.getRight() != null) {
			head = getLeftMost(head.getRight());
		} else {
			head = null;
		}
		return temp;
	}

	private Node<T> getRightMost(Node<T> root){
		boolean found = false;
		Node<T> right = root.getLeft();
		Node<T> parent = root;
		while(!found){
			if(right != null){
				if(right.getRight()!= null){
					parent = right;
					right = right.getRight();
				}
				else {
					if(right.getLeft() != null){
						parent.setRight(right.getLeft());
					}
					found = true;
				}
			}
			else {
				found = true;
			}
		}
		return right;
	}
	
	private Node<T> getLeftMost(Node<T> root){
		boolean found = false;
		Node<T> Left = root.getRight();
		Node<T> parent = root;
		while(!found){
			if(Left != null) {
				if(Left.getLeft()!= null){
					parent = Left;
					Left = Left.getLeft();
				}
				else {
					if(Left.getLeft() != null){
						parent.setLeft(Left.getRight());
					}
					found = true;
				}
			}
			else {
				found = true;
			}
		}
		return Left;
	}
	
	private void balanceTree(Node<T> root, Node<T> parent){
		int balanceFactor = getHeight(root.getLeft()) - getHeight(root.getRight());
		if(balanceFactor == 2) {
			Node<T> leftChild = root.getLeft();
			if((getHeight(leftChild.getLeft()) - getHeight(leftChild.getRight())) == -1) {
				rotateLeft(root.getLeft(), root);
			}
			rotateRight(root, parent);
		}
		else if(balanceFactor == -2) {
			Node<T> RightChild = root.getRight();
			if((getHeight(RightChild.getLeft()) - getHeight(RightChild.getRight())) == 1) {
				rotateRight(root.getRight(), root);
			}
			rotateLeft(root, parent);
		}
	}
	
	private int getHeight(Node<T> root) {
		if (root == null)
	    {
	        return 0;
	    }
	    else
	    {
	        return 1 +
	        Math.max(getHeight(root.getLeft()),
	        		getHeight(root.getRight()));
	    }
	}
	
	private void rotateLeft(Node<T> root, Node<T> parent){
		Node<T> rightChild = root.getRight();
		if(parent != null){
			if(root.getValue().compareTo(parent.getValue()) == 1){
				parent.setRight(rightChild);
			}
			else {
				parent.setLeft(rightChild);
			}
		}
		else {
			head = rightChild;
		}
		if(rightChild.getLeft()!=null) {
			root.setRight(rightChild.getLeft());
		}
		else {
			root.setRight(null);
		}
		rightChild.setLeft(root);	
	}
	
	private void rotateRight(Node<T> root, Node<T> parent){
		Node<T> leftChild = root.getLeft();
		if(parent != null){
			if(root.getValue().compareTo(parent.getValue()) == -1){
				parent.setLeft(leftChild);
			}
			else {
				parent.setRight(leftChild);
			}
		}
		else {
			head = leftChild;
		}
		if(leftChild.getRight()!=null) {
			root.setLeft(leftChild.getRight());
		}
		else {
			root.setLeft(null);
		}
		leftChild.setRight(root);
	}
}
