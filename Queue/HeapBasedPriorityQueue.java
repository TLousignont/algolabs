import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


public class HeapBasedPriorityQueue<T extends Comparable<T>> {

	Class<T> type; 
	List Heap;
	int tail = 1;
	int lenght = 0;
	@SuppressWarnings("unchecked")
	public HeapBasedPriorityQueue(Class<T> c, int initalSize) {
		lenght = nearestPow2(initalSize);
		Heap = new ArrayList<T>(lenght);
		for(int i = 0; i < lenght; i++){
			Heap.add(lenght);
		}
	}
	
	public int getSize(){
		return lenght;
	}
	
	private int nearestPow2(int value) {
		value--;
		value |= value >> 1;
		value |= value >> 2; 
		value |= value >> 4;
		value |= value >> 8;
		value |= value >> 16;
		value++;
		return value;
	}
	
	@SuppressWarnings("unchecked")
	public boolean offer(T data){
		if(tail + 1 >= lenght) {
			int initalSize = tail +1;
			lenght = nearestPow2(initalSize);
			List temp = new ArrayList<T>(lenght);
			for(int i = 0; i < lenght; i++){
				temp.add(lenght);
			}
			for(int i = 0; i < Heap.size(); i ++){
				temp.set(i, Heap.get(i));
			}
			Heap = temp;
		}
		Heap.set(tail, data);
		siftUP(tail);
		tail++;
		return true;
	}
	
	public T peek(){
		return (T) Heap.get(1);
	}
	
	private void siftUP(int index){
		int indexPostion = index;
		while(((T)Heap.get(indexPostion)).compareTo((T)Heap.get(indexPostion/2)) == 1){
			T temp = (T) Heap.get(indexPostion/2);
			Heap.set(indexPostion/2, Heap.get(indexPostion));
			Heap.set(indexPostion, temp);
			indexPostion = indexPostion/2;
		}
	}

	private void swap(int first, int second) {
		T temp = (T) Heap.get(second);
		Heap.set(second, Heap.get(first));
		Heap.set(first, temp);
	}
	
	private void siftDown(int index){
		boolean finished = false;
		int indexPostion = index;
		while(!finished){
			int left = indexPostion * 2;
			int right = (indexPostion * 2) + 1;
			if(left < tail && right < tail){
				if(((T)Heap.get(indexPostion)).compareTo((T)Heap.get(left)) == -1){
					swap(indexPostion, left);
					indexPostion = left;
				}
				else if(((T)Heap.get(indexPostion)).compareTo((T)Heap.get(right)) == -1){
					swap(indexPostion, right);
					indexPostion = right;
				}
				else {
					finished = true;
				}
			}
			else {
				finished = true;
			}
		}
	}
	
	public T poll() {
		T temp = peek();
		Heap.set(1, Heap.get(tail-1));
		Heap.set(tail-1, temp);
		tail--;
		siftDown(1);
		return temp;
	}
}
